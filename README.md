# The Impact of Default Dependency and Collateralization on Asset Pricing and Credit Risk Modeling

A broad range of financial instruments bear credit risk. Credit risk may be unilateral, bilateral, or multilateral. Some instruments such as, loans, bonds, etc, by nature contain only unilateral credit risk because only the default risk of one party appears to be relevant, whereas some other instruments, such as, over the counter (OTC) derivatives, securities financing transactions (SFT), and credit derivatives, bear bilateral or multilateral credit risk because two or more parties are susceptible to default risk. This paper mainly discusses bilateral and multilateral credit risk modeling, with a particular focus on default dependency, as correlated credit risk is one of the greatest threats to global financial markets.

There are two primary types of models that attempt to describe default processes in the literature: structural models and reduced-form (or intensity) models. Many practitioners in the credit trading arena have tended to gravitate toward the reduced-from models given their mathematical tractability. They can be made consistent with the risk-neutral probabilities of default backed out from corporate bond prices or credit default swap (CDS) spreads/premia. 

Central to the reduced-form models is the assumption that multiple defaults are independent conditional on the state of the economy. In reality, however, the default of one party might affect the default probabilities of other parties. Collin-Dufresne et al. (2003) and Zhang and Jorion (2007) find that a major credit event at one firm is associated with significant increases in the credit spreads of other firms. Giesecke (2004), Das et al. (2006), and Lando and Nielsen (2010) find that a defaulting firm can weaken the firms in its network of business links. These findings have important implications for the management of credit risk portfolios, where default relationships need to be explicitly modeled.

The main drawback of the conditionally independent assumption or the reduced-form models is that the range of default correlations that can be achieved is typically too low when compared with empirical default correlations (see Das et al. (2007)). The responses to correct this weakness can be generally classified into two categories: endogenous default relationship approaches and exogenous default relationship approaches. 

The endogenous approaches include the contagion (or infectious) models and frailty models. The frailty models (see Duffie et al. (2009), Koopman et al. (2011), etc) describe default clustering based on some unobservable explanatory variables. In variations of contagion or infectious type models (see Davis and Lo (2001), Jarrow and Yu (2001), etc.), the assumption of conditional independence is relaxed and default intensities are made to depend on default events of other entities. Contagion and frailty models fill an important gap but at the cost of analytic tractability. They can be especially difficult to implement for large portfolios.

The exogenous approaches (see Li (2000), Laurent and Gregory (2005), Hull and White (2004), Brigo et al. (2011), etc) attempt to link marginal default probability distributions to the joint default probability distribution through some external functions. Due to their simplicity in use, the exogenous approaches become very popular in practice.

Collateralization is one of the most important and widespread credit risk mitigation techniques used in derivatives transactions. According the ISDA (2012), 71% of all OTC derivatives transactions are subject to collateral agreements. The use of collateral in the financial markets has increased sharply over the past decade, yet the research on collateralized valuation is relatively sparse. Previous studies seem to turn away from direct and detailed modeling of collateralization (see Fuijii and Takahahsi (2012)). For example, Johannes and Sundaresan (2007), and Fuijii and Takahahsi (2012) characterize collateralization via a cost-of-collateral instantaneous rate (or stochastic dividend or convenience yield). Piterbarg (2010) regards collateral as a regular asset in a portfolio and uses the replication approach to price collateralized contracts.

This paper presents a new framework for valuing defaultable financial instruments with or without collateral arrangements. The framework characterizes default dependencies exogenously, and models collateral processes directly based on the fundamental principals of collateral agreements. Some well-known risky valuation models in the markets, e.g., the CDS model, the risky interest rate swap (IRS) model (Duffie and Huang (1996)), can be viewed as special cases of this framework, when the default dependencies are ignored. 

IRSs and CDSs are two of the largest segments of the OTC derivatives market, collectively accounting for around two-thirds of both the notional amount and market value of all outstanding derivatives. Given this framework, we are able to analyze the value of IRSs with bilateral credit risk and look at how swap rates are affected by correlated default risk. Our study shows that counterparty default correlations have a relatively small impact on swap rates. Furthermore, we find that the value of a fully collateralized IRS is equal to the risk-free value. This conclusion is consistent with the current market practice in which market participants commonly assume fully collateralized swaps are risk-free.

We also study the value of CDS contracts with trilateral credit risk and assess how spreads depend on the risk of the buyer, seller, and reference entity in a CDS contract. In general, a CDS contract is used to transfer the credit risk of a reference entity from one party to another. The risk circularity that transfers one type of risk (reference credit risk) into another (counterparty credit risk) within the CDS market is a concern for financial stability. Some people claim that the CDS market has increased financial contagion or even propose an outright ban on these instruments.

The standard CDS pricing model in the market assumes that there is no counterparty risk. Although this oversimplified model may be accepted in normal market conditions, its reliability in times of distress has recently been questioned. In fact, counterparty risk has become one of the most dangerous threats to the CDS market. For some time now it has been realized that, in order to value a CDS properly, counterparty effects have to be taken into account (see ECB (2009)).

We bring the concept of comvariance into the area of credit risk modeling to capture the statistical relationship among three or more random variables. Comvariance was first introduced to economics by Deardorff (1982), who used this measurement to correlate three factors in international trading. Furthermore, we define a new statistics, comrelation, as a scaled version of comvariance. Accounting for default correlations and comrelations becomes important in determining CDS premia, especially during the credit crisis. Our analysis shows that the effect of default dependencies on CDS premia from large to small is i) the default correlation between the protection seller and the reference entity, ii) the default comrelation, iii) the default correlation between the protection buyer and the reference entity, and iv) the default correlation between the protection buyer and the protection seller. In particular, we find that the default comvariance/comrelation has substantial effects on the asset pricing and risk management, which have never been documented.

There is a significant increase in the use of collateral for CDS after the recent financial crises. Many people believe that, if a CDS is fully collateralized, there is no risk of failure to pay. Collateral posting regimes are originally designed and utilized for bilateral risk products, e.g., IRS, but there are many reasons to be concerned about the success of collateral posting in offsetting the risk of CDS contracts. First, the value of CDS contracts tends to move very suddenly with big jumps, whereas the price movement of IRS contracts is far smoother and less volatile than CDS. Second, CDS spreads can widen very rapidly. Third, CDS contracts have many more risk factors than IRS contracts. In fact, our model shows that full collateralization cannot eliminate counterparty risk completely for a CDS.

This article also shows that the pricing process of a defaultable instrument normally has a backward recursive nature if the payoff can be positive or negative. Accordingly, we propose a backward induction approach for risky valuation. In contrast to the popular recursive integral solution (see Duffie and Huang (1996)), our backward induction method significantly simplifies the implementation. One can make use of the well-established algorithms, such as lattice/tree and regression-based Monte Carlo, to price a defaultable instrument.

This article presents a new valuation framework for pricing financial instruments subject to credit risk. In particular, we focus on modeling default relationships. Some well-known risky valuation models in the market can be viewed as special cases of this framework, when the default dependencies are ignored.

To capture the default relationships among more than two defaultable entities, we introduce a new statistic: comrelation, an analogue to correlation for multiple variables, to exploit any multivariate statistical relationship. Our research shows that accounting for default correlations and comrelations becomes important, especially under market stress. The existing valuation models in the credit derivatives market, which take into account only pair-wise default correlations, may underestimate credit risk and may be inappropriate.

We study the sensitivity of the price of a defaultable instrument to changes in the joint credit quality of the parties. For instance, our analysis shows that the effect of default dependence on CDS premia from large to small is the correlation between the protection seller and the reference entity, the comrelation, the correlation between the protection buyer and the reference entity, and the correlation between the protection buyer and the protection seller.

The model shows that a fully collateralized swap is risk-free, while a fully collateralized CDS is not equivalent to a risk-free one. Therefore, we conclude that collateralization designed to mitigate counterparty risk works well for financial instruments subject to bilateral credit risk, but fails for ones subject to multilateral credit risk. 

References

Arora, Navneet, Priyank Gandhi, and Francis A. Longstaff (2009), “Counterparty credit risk and the credit default swap market,” Working paper, UCLA.

Brigo, D., A. Pallavicini, and R. Torresetti (2011), “Credit Models and the Crisis: default cluster dynamics and the Generalized Poisson Loss model,” Journal of Credit Risk, 6: 39-81.

Collin-Dufresne, P., R. Goldstein, and J. Helwege (2003), “Is credit event risk priced? Modeling contagion via the updating of beliefs,” Working paper, Haas School, University of California, Berkeley.

Das, S., D. Duffie, N. Kapadia, and L. Saita (2007), “Common failings: How corporate defaults are correlated,” Journal of Finance, 62: 93-117.

Das, S., L. Freed, G. Geng, N. Kapadia (2006), “Correlated default risk,” Journal of Fixed Income, 16: 7-32.

Davis, M., and V. Lo (2001), “Infectious defaults,” Quantitative Finance, 1: 382-387.

Deardorff, Alan V. (1982): “The general validity of the Heckscher-Ohlin Theorem,” American Economic Review, 72 (4): 683-694.

Duffie, D., A. Eckner, G. Horel, and L. Saita (2009), “Frailty correlated default,” Journal of Finance, 64: 2089-2123.

Duffie, D., and M. Huang (1996), “Swap rates and credit quality,” Journal of Finance, 51: 921-949.

Duffie, D., and K. Singleton (1999), “Modeling term structure of defaultable bonds,” Review of Financial Studies, 12: 687-720.

ECB (2009), “Credit default swaps and counterparty risk,” European central bank.

FinPricing, 2019, Market data services, https://finpricing.com/lib/FxVolIntroduction.html

Fuijii, M. and A. Takahahsi (2012), “Collateralized CDS and default dependence – Implications for the central clearing,” Journal of Credit Risk, 8(3): 97-113.

Giesecke, K. (2004), “Correlated default with incomplete information,” Journal of Banking and Finance, 28: 1521-1545.

Hull, J. and A. White (2004), “Valuation of a CDO and a nth to default CDS without Monte Carlo simulation,” Journal of Derivatives, 12: 8-23.

ISDA (2012), “ISDA margin survey 2012.”

Jarrow, R., and S. Turnbull (1995), “Pricing derivatives on financial securities subject to credit risk,” Journal of Finance, 50: 53-85.

Jarrow, R., and F. Yu (2001), “Counterparty risk and the pricing of defaultable securities,” Journal of Finance, 56: 1765-99.

Johannes, M. and S. Sundaresan (2007), “The impact of collateralization on swap rates,” Journal of Finance, 62: 383-410.

J. P. Morgan (1999), “The J. P. Morgan guide to credit derivatives,” Risk Publications.

Koopman, S., A. Lucas, and B. Schwaab (2011), “Modeling frailty-correlated defaults using many macroeconomic covariates,” Journal of Econometrics, 162: 312-325.

Lando, D. and M. Nielsen (2010), “Correlation in corporate defaults: contagion or conditional independence?” Journal of Financial Intermediation, 19: 355-372.

Laurent, J. and J. Gregory (2005), “Basket default swaps, CDOs and factor copulas,” Journal of Risk, 7:  
03-122.

Li, D. (2000), “On default correlation: A copula function approach,” Journal of Fixed Income, 9: 43-54.

Longstaff, F., and E. Schwartz (2001): “Valuing American options by simulation: a simple least-squares approach,” The Review of Financial Studies, 14 (1): 113-147.

Madan, D., and H. Unal (1998), “Pricing the risks of default,” Review of Derivatives Research, 2: 121-160.

Moody’s Investor’s Service (2000): “Historical default rates of corporate bond issuers, 1920-99,”

O’Kane, D. and S. Turnbull (2003), “Valuation of credit default swaps,” Fixed Income Quantitative Credit Research, Lehman Brothers, QCR Quarterly, Q1/Q2: 1-19.

Piterbarg, V. (2010), “Funding beyond discounting: collateral agreements and derivatives pricing,” Risk Magazine, 2010 (2): 97-102.

Teugels, J. (1990), “Some representations of the multivariate Bernoulli and binomial distributions,” Journal of Multivariate Analysis, 32: 256-268.

Xiao, T. (2011), “An efficient lattice algorithm for the LIBOR market model,” Journal of Derivatives, 19 (1): 25-40.

Zhang, G., and P. Jorion, (2007), “Good and bad credit contagion: Evidence from credit default swaps,” Journal of Financial Economics 84: 860–883.


